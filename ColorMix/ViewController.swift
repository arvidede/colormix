//
//  ViewController.swift
//  ColorMix
//
//  Created by Arvid Edenheim on 2017-05-10.
//  Copyright © 2017 Arvid Edenheim. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        updateColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateColor() {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        
        if redSwitch.isOn {
            red = CGFloat(redSlider.value)
        }
        
        if greenSwitch.isOn {
            green = CGFloat(greenSlider.value)
        }
        
        if blueSwitch.isOn {
            blue = CGFloat(blueSlider.value)
        }
        
        colorView.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: 1)
    }

    @IBAction func switchChanged(_ sender: UISwitch) {
        updateColor()
    }
    
    @IBAction func sliderChanged(_ sender: UISlider) {
        updateColor()
    }
    @IBOutlet weak var colorView: UIView!
    
    @IBOutlet weak var blueSlider: UISlider!
    
    @IBOutlet weak var greenSlider: UISlider!
    
    @IBOutlet weak var redSlider: UISlider!
    
    @IBOutlet weak var blueSwitch: UISwitch!

    @IBOutlet weak var greenSwitch: UISwitch!
    
    @IBOutlet weak var redSwitch: UISwitch!
    
}

